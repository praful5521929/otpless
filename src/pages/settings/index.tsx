import { Button } from "antd";
import React, { useEffect, useState } from "react";

export default function Settings() {
  const [userData, setUserData] = useState<any>([]);
  const [loading, setLoading] = useState<any>(true);
  const [error, setError] = useState<any>(null);
  const onAdd = async () => {
    try {
      const response = await fetch(
        "https://jsonplaceholder.typicode.com/users",
        {
          method: "POST",
          body: JSON.stringify({
            name: "praful",
            email: "praful@gmail.com",
          }),
          headers: {
            "Content-Type": "application/json; charset=utf-8",
          },
        }
      );

      if (response.status !== 201) {
        alert("Failed to create user. Status code: " + response.status);
        return;
      }

      const data = await response.json();
      console.log(data);
      // You can set the user data to state here if needed.
      setUserData((users:any)=>[...users,data]);
    } catch (error) {
      console.error("Error creating user:", error);
    }
  };

  useEffect(() => {
    const fetchUser = async () => {
      try {
        const response = await fetch(
          "https://jsonplaceholder.typicode.com/users"
        );
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        const userData = await response.json();
        setUserData((users:any)=>userData);
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchUser();
  }, []);
  return (
    <div>
      <Button type="primary" onClick={onAdd}>Add user</Button>
      <div>
        <h1>User Data</h1>
        {loading
          ? "loading..."
          : userData && (
              <div>
                {userData.map((val: any, index: number) => {
                  return (
                    <>
                      <p>Name: {val.name}</p>
                      <p>Email: {val.email}</p>
                    </>
                  );
                })}
                {/* Render other user data properties as needed */}
              </div>
            )}
      </div>
    </div>
  );
}
