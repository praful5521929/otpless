import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from '../../redux/store'
import { Button, Row, Space } from 'antd'
import { decrement, increment } from '../../redux/features/counter/counterSlice'
import { BlogState, fetchBlogPosts } from '../../redux/features/blogpostget'
import BlogCard from '../../components/blogCard'

export default function Blog() {
    const dispatch: AppDispatch = useDispatch();
    const blogPosts = useSelector((state: any) => state?.blogpots?.posts); // Use the BlogState type to access the posts array
  
    useEffect(() => {
      dispatch(fetchBlogPosts());
    }, [dispatch]);
    
// useEffect(() => {
//   const script = document.createElement('script');
//   script.type = 'text/javascript';
//   script.src = 'https://otpless.com/auth.js';
//   script.async = true;

//   script.onload = () => {
//     console.log('Script loaded successfully.');
//     console.log('Window.otpless:', window.otpless); // Check if otpless is present in the window object.
//   };

//   script.onerror = () => {
//     console.error('Error loading script.');
//   };

//   document.body.appendChild(script);

//   window.otpless = (otplessUser) => {
//     console.log('otplessUser:', otplessUser);
//   };

//   return () => {
//     document.body.removeChild(script);
//   };
// }, []);



                
  return (
  <>
  <div className='cont'>
    <p className='text-color'>demo text</p>
  </div>
        <Row gutter={[24,24]}>
        {
            blogPosts?.map((val:any,index:number)=>{
                return  <BlogCard data={val}/>
            })
        }
    </Row>
  </>
  )
}
