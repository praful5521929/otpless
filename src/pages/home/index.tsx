import React, { useState } from "react";
import PostList from "../../components/postList";
import { Col, Input, Row } from "antd";

const { Search } = Input;
export default function Home() {
  const onSearch = (value: string) => console.log(value);

  return (
    <div>
      <Row>
        <Col>
          <Search
            placeholder="input search text"
            allowClear
            enterButton="Search"
            size="large"
            onSearch={onSearch}
          />
        </Col>
      </Row>
      <PostList />
    </div>
  );
}
