import React from 'react';
import logo from './logo.svg';
import './App.css';
import Main from './components/Main';
import { Col, Row } from 'antd';

function App() {
  return (
    <div className="App">

        <header className=''>
          <div className='header d-flex p-5 py-0 justify-between align-center'>
          <div>
            <h1>Blog App</h1>
          </div>
          <div className='d-flex'>
              <p className='m-2'>home</p>
              <p className='m-2'>contact</p>
              <p className='m-2'>blog</p>
          </div>
          </div>
      </header>
      <Row justify="center"> 
        <Col span={22}>
          <Main />
        </Col>
      </Row>
    </div>
  );
}

export default App;
