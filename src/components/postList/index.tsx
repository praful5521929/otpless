import React, { useEffect, useReducer, useState } from 'react'
import PostCard from '../postcard';
import { PostProps } from '../../types/types';
import { INITIAL_STATE, postReducer } from '../../reducer/PostReducer';
import { ACTION_TYPE } from '../../stateTypes/postActionType';

export const fetchPosts = async () => {
    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/posts');
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return await response.json();
    } catch (error) {
      console.error('Error fetching posts:', error);
      return [];
    }
  };
export default function PostList() { 
    const [state, dispatch] = useReducer(postReducer, INITIAL_STATE)

    useEffect(() => {
        // setLoading(true)
        dispatch({type:ACTION_TYPE.FETCH_START})
        const getPosts = async () => {
          try {
            const data = await fetchPosts();
            dispatch({ type: ACTION_TYPE.FETCH_SUCCESS ,payload:data});
          } catch (error) {
            // Handle the error here, you can log it or show an error message
            console.error('Error fetching posts:', error);
            dispatch({ type: ACTION_TYPE.FETCH_ERROR });
          }
        };        
        getPosts();
      }, []);


      
  return (
    <div>
        {
         state?.loading?"loading...":state?.post.map((posts:PostProps,index:number):any=>{
                return <PostCard key={index} {...posts} />
            })
        }
    </div>
  )
}
