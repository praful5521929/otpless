import { Card, Col, Row ,Avatar} from "antd";
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import React from "react";
import { Link } from "react-router-dom";
const { Meta } = Card;
export default function BlogCard(props:any) {
    const {data}=props
    console.log(data);
    
  return (
    <Col lg={8} sm={12} xs={24}>
      <Link to={`/blog/${data?.id}`}>
      <Card
        cover={
          <img
            alt="example"
            src={`https://source.unsplash.com/${Math.random()*10}`}
          />
        }
        actions={[
          <SettingOutlined key="setting" />,
          <EditOutlined key="edit" />,
          <EllipsisOutlined key="ellipsis" />,
        ]}
      >
        <Meta
          avatar={
            <Avatar src="https://xsgames.co/randomusers/avatar.php?g=pixel" />
          }
          title={data?.title}
          description={data?.body}
        />        
      </Card>
      </Link>
    </Col>
  );
}
