import React from 'react'
import PostList from '../postList';
import { PostProps } from '../../types/types';

export default function PostCard(props:PostProps) {
    console.log(props);
    
  return (
    <div>
        <h1>{props?.title}</h1>
        <p>{props?.body}</p>
        {/* <PostList /> */}
    </div>
  )
}
