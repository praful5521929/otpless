import React from 'react'
import { Route, Routes } from 'react-router-dom'
import Home from '../pages/home'
import Settings from '../pages/settings'
import Topics from '../pages/topics'
import Blog from '../pages/blog'
import BlogDetails from '../pages/BlogDetails'

export default function Main() {
  return (
    <Routes>
        <Route path='/' element={<Home/>} />
        <Route path='/topics' element={<Topics/>} />
        <Route path='/settings' element={<Settings/>} />
        <Route path='/blog' element={<Blog />} />
        <Route path='/blog/:id' element={<BlogDetails />} />
    </Routes>
  )
}
