// blogSlice.ts

import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export interface BlogState {
  posts: any[];
}

const initialState: BlogState = {
  posts: [],
}

export const fetchBlogPostsDetails = createAsyncThunk(
  'fetchblog/fetchPosts',
  async () => {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts');
    if (!response.ok) {
      throw new Error('Failed to fetch blog posts');
    }
    const data = await response.json();
    return data;
  }
);

const blogSlice = createSlice({
  name: 'fetchblog',
  initialState,
  reducers: {

  },
  extraReducers: (builder) => {
    builder.addCase(fetchBlogPostsDetails.fulfilled, (state, action) => {
      state.posts = action.payload;
    });
  },
});

export default blogSlice.reducer;
