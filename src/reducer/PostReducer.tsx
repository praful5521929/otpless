import { ACTION_TYPE } from "../stateTypes/postActionType";

type postReducerStateType={
    loading:boolean,
    post:any[],
    error:boolean
}

type postReducerActionType={
    type:"FETCH_START " | "FETCH_SUCCESS" | "FETCH_ERROR",
    payload:any
}

export const INITIAL_STATE = {
  loading: false,
  post: [],
  error: false,
};

export const postReducer = (state:any,action:any) => {
  switch (action.type) {
    case ACTION_TYPE.FETCH_START:
      return {
        loading: true,
        error: false,
        post: [],
      };
    case ACTION_TYPE.FETCH_SUCCESS:
      return {
        loading: false,
        post: action.payload,
      };
    case ACTION_TYPE.FETCH_ERROR:
      return {
        error: true,
        loading: false,
        post: []
      };
    default:
        return state
  }
};
